mainFile = src/Main.elm
outputFile = elm.js
output = --output=$(outputFile)
elmMakeCmd = elm make $(mainFile) $(output)
defaultApiUrl = http://localhost:8080
APIURL ?= $(defaultApiUrl)

build:
	$(elmMakeCmd)
	@sed -i 's|$(defaultApiUrl)|$(APIURL)|g' $(outputFile)
docker: build
	docker build .
debug:
	$(elmMakeCmd) --debug
live:
	elm-live $(mainFile) --pushstate -- $(output)