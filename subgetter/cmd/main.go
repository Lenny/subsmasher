package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
)

type Video struct {
	Title   string `json:"title"`
	Channel string `json:"channel"`
	ID      string `json:"id"`
}

type handler struct {
	parser *gofeed.Parser
	url    string
}

func responseErr(w http.ResponseWriter, err error) {
	log.Println(err)
	w.WriteHeader(http.StatusInternalServerError)
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	feed, err := h.parser.ParseURL(h.url)
	if err != nil {
		responseErr(w, err)
		return
	}

	videos := make([]Video, len(feed.Items))
	for idx, item := range feed.Items {
		videos[idx] = Video{
			Title:   item.Title,
			Channel: item.Author.Name,
			ID:      strings.ReplaceAll(item.GUID, "yt:video:", ""),
		}
	}
	data, err := json.Marshal(videos)

	if err != nil {
		responseErr(w, err)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*") // TODO: make configurable
	_, err = w.Write(data)
	if err != nil {
		responseErr(w, err)
		return
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("usage: subgetter <full invidious rss feed url>")
		return
	}

	router := handler{
		gofeed.NewParser(),
		os.Args[1],
	}

	s := &http.Server{
		Addr:           ":8080",
		Handler:        &router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
}
