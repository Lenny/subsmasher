module OrderedDictExtra exposing (update)

import OrderedDict exposing (OrderedDict)


update : comparable -> a -> OrderedDict comparable a -> OrderedDict comparable a
update key value dict =
    OrderedDict.update key (\_ -> Just value) dict
