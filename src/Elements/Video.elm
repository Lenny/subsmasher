module Elements.Video exposing (..)

import Css exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, src)


flexVideoPlayer : String -> Html msg
flexVideoPlayer videoID =
    div [ css [ flexGrow (int 1), position relative ] ]
        [ img [ src "res/16x9.png", css [ position absolute, width (pct 100), height (pct 100) ] ] []
        , iframe
            -- iframes
            [ src ("https://invidious.degoog.lenny.ninja/embed/" ++ videoID ++ "?quality=dash")
            , css
                [ position absolute
                , top (px 0)
                , left (px 0)
                , height (pct 100)
                , width (pct 100)
                , border (px 0)
                ]
            ]
            []
        ]


videoPlayer : List Style -> String -> Html msg
videoPlayer styles videoID =
    div [ css (position relative :: styles) ]
        [ img [ src "res/16x9.png", css [ position absolute, width (pct 100), height (pct 100) ] ] []
        , iframe
            -- iframes
            [ src ("https://invidious.degoog.lenny.ninja/embed/" ++ videoID)
            , css
                [ position absolute
                , top (px 0)
                , left (px 0)
                , height (pct 100)
                , width (pct 100)
                , border (px 0)
                ]
            ]
            []
        ]
