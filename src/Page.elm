module Page exposing (view)

import Browser exposing (Document)
import Css exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, href)


view : { title : String, content : Html msg } -> Document msg
view { title, content } =
    { title = title ++ " - Subsmasher"
    , body = [ toUnstyled viewHeader, toUnstyled content ]
    }


viewHeader : Html msg
viewHeader =
    nav [ css [ height (vh 2.5) ] ] [ header [] [ a [ href "/" ] [ text "Subsmasher" ] ] ]
