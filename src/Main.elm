module Main exposing (Msg(..), main)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Json.Decode
import OrderedDict
import Page as Page
import Page.Blank as Blank
import Page.Home as Home
import Page.List as List
import Page.Watcher as Watcher
import Route exposing (Route(..))
import Url exposing (Url)
import Videos exposing (VideoStorage, videoStorageDecoder)



-- MODEL


type PageModel
    = Redirect VideoStorage
    | Home Home.Model
    | List List.Model
    | Watcher Watcher.Model


type alias Model =
    { key : Nav.Key
    , pageModel : PageModel
    }


init : Maybe Json.Decode.Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init maybeStoredData url key =
    changeRouteTo (Route.fromUrl url)
        (Model key (Redirect (decodeVideoToData maybeStoredData)))



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        viewPage toMsg config =
            let
                { title, body } =
                    Page.view config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model.pageModel of
        Redirect _ ->
            Page.view Blank.view

        Home home ->
            viewPage GotHomeMsg (Home.view home)

        List list ->
            viewPage GotListMsg (List.view list)

        Watcher watcher ->
            viewPage GotWatcherMsg (Watcher.view watcher)



-- UPDATE


type Msg
    = ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | GotHomeMsg Home.Msg
    | GotListMsg List.Msg
    | GotWatcherMsg Watcher.Msg


decodeVideoToData : Maybe Json.Decode.Value -> VideoStorage
decodeVideoToData maybeData =
    case maybeData of
        Nothing ->
            VideoStorage OrderedDict.empty OrderedDict.empty

        Just data ->
            case Json.Decode.decodeValue videoStorageDecoder data of
                Ok storage ->
                    storage

                Err _ ->
                    VideoStorage OrderedDict.empty OrderedDict.empty


toVideoStorage : PageModel -> VideoStorage
toVideoStorage model =
    case model of
        Home home ->
            home.storage

        Redirect data ->
            data

        List list ->
            list.storage

        Watcher home ->
            home.storage


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        videoStorage =
            toVideoStorage model.pageModel
    in
    case maybeRoute of
        Nothing ->
            Home.init videoStorage
                |> updateWith Home GotHomeMsg model

        Just Route.Home ->
            Home.init videoStorage
                |> updateWith Home GotHomeMsg model

        Just Route.Queue ->
            List.init List.Queue videoStorage
                |> updateWith List GotListMsg model

        Just Route.Skipped ->
            List.init List.Skipped videoStorage
                |> updateWith List GotListMsg model

        Just Route.Watcher ->
            Watcher.init videoStorage
                |> updateWith Watcher GotWatcherMsg model


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.pageModel ) of
        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ( ChangedUrl url, _ ) ->
            changeRouteTo (Route.fromUrl url) model

        ( GotHomeMsg subMsg, Home home ) ->
            Home.update subMsg home
                |> updateWith Home GotHomeMsg model

        ( GotListMsg subMsg, List list ) ->
            List.update subMsg list
                |> updateWith List GotListMsg model

        ( GotWatcherMsg subMsg, Watcher watcher ) ->
            Watcher.update subMsg watcher
                |> updateWith Watcher GotWatcherMsg model

        ( _, _ ) ->
            -- Disregard messages that arrived for the wrong page.
            ( model, Cmd.none )


updateWith : (subModel -> PageModel) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( { model | pageModel = toModel subModel }
    , Cmd.map toMsg subCmd
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.pageModel of
        Home home ->
            Sub.map GotHomeMsg (Home.subscriptions home)

        List _ ->
            Sub.none

        Redirect _ ->
            Sub.none

        Watcher _ ->
            Sub.none


main : Program (Maybe Json.Decode.Value) Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        }
