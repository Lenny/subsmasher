module Videos exposing (Video, VideoStorage, getSubscriptions, saveVideos, videoStorageDecoder)

import Array
import Html exposing (video)
import Http
import Json.Decode as Decode exposing (Decoder, field)
import Json.Encode as Encode
import OrderedDict exposing (OrderedDict)
import Ports


type alias VideoStorage =
    { queue : OrderedDict String Video
    , skipped : OrderedDict String Video
    }


listToVideoDictFolder : Video -> OrderedDict String Video -> OrderedDict String Video
listToVideoDictFolder video videoDict =
    OrderedDict.insert video.id video videoDict


listToVideoDictDecoder : Decoder (OrderedDict String Video)
listToVideoDictDecoder =
    Decode.list videoDecoder
        |> Decode.andThen
            (\videos ->
                List.foldl listToVideoDictFolder OrderedDict.empty videos
                    |> Decode.succeed
            )


videoStorageDecoder : Decoder VideoStorage
videoStorageDecoder =
    Decode.map2
        VideoStorage
        (field "queue" listToVideoDictDecoder)
        (field "skipped" listToVideoDictDecoder)


videoEncoder : Video -> Encode.Value
videoEncoder video =
    Encode.object
        [ ( "title", Encode.string video.title )
        , ( "channel", Encode.string video.channel )
        , ( "id", Encode.string video.id )
        ]


orderedVideoDictEncoder : OrderedDict String Video -> Encode.Value
orderedVideoDictEncoder orderedDict =
    Encode.list videoEncoder
        (OrderedDict.values orderedDict)


videoStorageEncoder : VideoStorage -> Encode.Value
videoStorageEncoder videos =
    Encode.object
        [ ( "queue", orderedVideoDictEncoder videos.queue )
        , ( "skipped", orderedVideoDictEncoder videos.skipped )
        ]


saveVideos : VideoStorage -> Cmd msg
saveVideos videos =
    videoStorageEncoder videos
        |> Encode.encode 0
        |> Ports.storeVideos


videoDecoder : Decoder Video
videoDecoder =
    Decode.map3
        Video
        (field "title" Decode.string)
        (field "channel" Decode.string)
        (field "id" Decode.string)


subscriptionsDecoder : Decoder (Array.Array Video)
subscriptionsDecoder =
    Decode.array videoDecoder


apiUrl : String
apiUrl =
    "http://localhost:8080"


getSubscriptions : (Result Http.Error (Array.Array Video) -> msg) -> Cmd msg
getSubscriptions thing =
    Http.get
        { url = apiUrl
        , expect = Http.expectJson thing subscriptionsDecoder
        }


type alias Video =
    { title : String
    , channel : String
    , id : String
    }
