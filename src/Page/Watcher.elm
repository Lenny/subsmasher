module Page.Watcher exposing (Model, Msg, init, update, view)

import Array exposing (Array)
import Css exposing (..)
import Elements.Video exposing (videoPlayer)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css)
import Html.Styled.Events exposing (onClick)
import OrderedDict exposing (OrderedDict)
import Videos exposing (Video, VideoStorage, saveVideos)


type alias Model =
    { storage : VideoStorage
    , currentVideo : Int
    }


init : VideoStorage -> ( Model, Cmd Msg )
init storage =
    ( Model storage 0, Cmd.none )



-- UPDATE


type Msg
    = QueueRemoved Video
    | Next
    | Previous


removeVideoFromQueue : VideoStorage -> String -> VideoStorage
removeVideoFromQueue storage id =
    { storage | queue = OrderedDict.remove id storage.queue }


nextVideoIdx : Array Video -> Int -> Int -> Int
nextVideoIdx videos step idx =
    let
        nextIdx =
            idx + step
    in
    case Array.get nextIdx videos of
        Nothing ->
            idx

        Just _ ->
            nextIdx


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        QueueRemoved video ->
            let
                newModel =
                    { model
                        | storage = removeVideoFromQueue model.storage video.id
                    }
            in
            ( newModel, saveVideos (VideoStorage newModel.storage.queue newModel.storage.skipped) )

        Next ->
            let
                newModel =
                    { model
                        | currentVideo =
                            nextVideoIdx
                                (queueToArray model.storage.queue)
                                1
                                model.currentVideo
                    }
            in
            ( newModel, Cmd.none )

        Previous ->
            let
                newModel =
                    { model
                        | currentVideo =
                            nextVideoIdx
                                (queueToArray model.storage.queue)
                                -1
                                model.currentVideo
                    }
            in
            ( newModel, Cmd.none )



-- VIEW


flexButton : List (Attribute msg) -> List (Html msg) -> Html msg
flexButton attributes children =
    button (css [ flexBasis (pct 20) ] :: attributes) children


queueToArray : OrderedDict String Video -> Array Video
queueToArray orderedDict =
    Array.fromList (OrderedDict.values orderedDict)


view : Model -> { title : String, content : Html Msg }
view model =
    { title = ""
    , content =
        case Array.get model.currentVideo (queueToArray model.storage.queue) of
            Nothing ->
                text "queue is empty"

            Just currentVideo ->
                div
                    []
                    [ videoPlayer [ height (vh 97.5), width (pct 100) ] currentVideo.id
                    , div [ css [ displayFlex, justifyContent spaceAround ] ]
                        [ flexButton [ onClick Previous ] [ text "back" ]
                        , flexButton [ onClick (QueueRemoved currentVideo) ] [ text "skip" ]
                        , flexButton [ onClick Next ] [ text "forward" ]
                        ]
                    ]
    }
