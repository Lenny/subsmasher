module Page.Blank exposing (view)

import Html.Styled exposing (..)


view : { title : String, content : Html msg }
view =
    { title = ""
    , content = text "blank"
    }
