module Page.Home exposing (Model, Msg, init, subscriptions, update, view)

import Array exposing (Array)
import Css exposing (..)
import Debug exposing (toString)
import Elements.Video exposing (flexVideoPlayer)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (action, css, type_, value)
import Html.Styled.Events exposing (onClick)
import Http
import OrderedDict exposing (OrderedDict)
import OrderedDictExtra
import OrderedSet exposing (OrderedSet)
import Route
import Videos exposing (Video, VideoStorage, getSubscriptions, saveVideos)


type alias Model =
    { videos : Array.Array Video
    , currentVideo : Int
    , storage : VideoStorage
    }


emptyModel : Model
emptyModel =
    Model Array.empty 0 (VideoStorage OrderedDict.empty OrderedDict.empty)


init : VideoStorage -> ( Model, Cmd Msg )
init storedData =
    ( { emptyModel | storage = storedData }
    , getSubscriptions GotSubscriptions
    )


type Msg
    = GotSubscriptions (Result Http.Error (Array.Array Video))
    | Next
    | Previous
    | QueueAdded Video
    | Skipped Video


type VideoAvailability
    = Idx Int
    | OutOfRange


nextIdxAvailableRec : Array.Array Video -> OrderedSet String -> Int -> Int -> VideoAvailability
nextIdxAvailableRec videos skippedVideos nextIdx step =
    case Array.get nextIdx videos of
        Nothing ->
            OutOfRange

        Just video ->
            if OrderedSet.member video.id skippedVideos then
                nextIdxAvailableRec videos skippedVideos (nextIdx + step) step

            else
                Idx nextIdx


nextIdxAvailable : Array.Array Video -> OrderedSet String -> Int -> Int -> Int
nextIdxAvailable videos skippedVideos idx step =
    let
        nextIdx =
            idx + step
    in
    case nextIdxAvailableRec videos skippedVideos nextIdx step of
        OutOfRange ->
            idx

        Idx result ->
            result


nextVideoIdx : Array.Array Video -> OrderedSet String -> Int -> Int
nextVideoIdx videos skippedVideos idx =
    nextIdxAvailable videos skippedVideos idx 1


prevVideoIdx : Array.Array Video -> OrderedSet String -> Int -> Int
prevVideoIdx videos skippedVideos idx =
    nextIdxAvailable videos skippedVideos idx -1


setOfKeysFromDict : OrderedDict comparable v -> OrderedSet comparable
setOfKeysFromDict dict =
    OrderedSet.fromList (OrderedDict.keys dict)


updateVideos : Model -> Array Video -> ( Model, Cmd msg )
updateVideos model videos =
    let
        skippedSet =
            setOfKeysFromDict model.storage.skipped
    in
    ( { model
        | videos = videos
        , currentVideo =
            nextVideoIdx videos skippedSet model.currentVideo
                |> prevVideoIdx videos skippedSet

        -- dirty hack to be able to skip initial video
      }
    , Cmd.none
    )


modelWithNextVideo : Model -> Model
modelWithNextVideo model =
    { model
        | currentVideo =
            nextVideoIdx
                model.videos
                (setOfKeysFromDict model.storage.skipped)
                model.currentVideo
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotSubscriptions result ->
            case result of
                Ok videos ->
                    updateVideos model videos

                Err _ ->
                    ( model, Cmd.none )

        Next ->
            ( modelWithNextVideo model, Cmd.none )

        Previous ->
            ( { model
                | currentVideo =
                    prevVideoIdx
                        model.videos
                        (setOfKeysFromDict model.storage.skipped)
                        model.currentVideo
              }
            , Cmd.none
            )

        QueueAdded video ->
            let
                storage =
                    model.storage
            in
            let
                newModel =
                    { model
                        | storage = { storage | queue = OrderedDictExtra.update video.id video storage.queue }
                    }
            in
            ( modelWithNextVideo newModel
            , saveVideos (VideoStorage newModel.storage.queue newModel.storage.skipped)
            )

        Skipped video ->
            let
                storage =
                    model.storage
            in
            let
                newModel =
                    { model
                        | storage = { storage | skipped = OrderedDictExtra.update video.id video storage.skipped }
                    }
            in
            ( modelWithNextVideo newModel
            , saveVideos (VideoStorage newModel.storage.queue newModel.storage.skipped)
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


flexButton : List (Attribute msg) -> List (Html msg) -> Html msg
flexButton attributes children =
    button (css [ flexBasis (pct 20) ] :: attributes) children


flexLinkButton : Route.Route -> String -> List (Attribute msg) -> Html msg
flexLinkButton route text attributes =
    form
        (action (Route.routeToString route) :: attributes)
        [ input [ type_ "submit", value text, css [ maxWidth (vw 30) ] ] []
        ]


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "home"
    , content =
        case Array.get model.currentVideo model.videos of
            Nothing ->
                h1 [] [ text "error" ]

            Just currentVideo ->
                div
                    [ css
                        [ displayFlex
                        , flexDirection column
                        , height (vh 97.5)
                        , width (vw 100)
                        , justifyContent spaceBetween
                        ]
                    ]
                    [ div
                        [ css [ flexGrow (int 1), displayFlex ] ]
                        [ flexVideoPlayer currentVideo.id ]
                    , div [ css [ flexShrink (int 0) ] ]
                        [ div [ css [ displayFlex, justifyContent spaceAround ] ]
                            [ flexButton [ onClick Previous ] [ text "back" ]
                            , flexButton [ onClick (QueueAdded currentVideo) ] [ text "add to queue" ]
                            , flexButton [ onClick (Skipped currentVideo) ] [ text "skip" ]
                            , flexButton [ onClick Next ] [ text "forward" ]
                            ]
                        , div [ css [ displayFlex, justifyContent spaceAround ] ]
                            [ flexLinkButton Route.Queue ("queue (" ++ toString (OrderedDict.size model.storage.queue) ++ ")") []
                            , flexLinkButton Route.Watcher "play queue" []
                            , flexLinkButton Route.Skipped ("skipped (" ++ toString (OrderedDict.size model.storage.skipped) ++ ")") []
                            ]
                        ]
                    ]
    }
