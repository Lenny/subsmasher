module Page.List exposing (ListKind(..), Model, Msg, init, update, view)

import Css exposing (displayFlex, flexBasis, flexGrow, int, pct)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css)
import Html.Styled.Events exposing (onClick)
import OrderedDict exposing (OrderedDict)
import Videos exposing (Video, VideoStorage, saveVideos)


type ListKind
    = Queue
    | Skipped


type alias Model =
    { storage : VideoStorage
    , kind : ListKind
    }


init : ListKind -> VideoStorage -> ( Model, Cmd Msg )
init kind storage =
    ( Model storage kind, Cmd.none )



-- UPDATE


type Msg
    = QueueRemoved Video


removeVideoFromStorage : ListKind -> VideoStorage -> String -> VideoStorage
removeVideoFromStorage kind storage id =
    case kind of
        Queue ->
            { storage | queue = OrderedDict.remove id storage.queue }

        Skipped ->
            { storage | skipped = OrderedDict.remove id storage.skipped }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        QueueRemoved video ->
            let
                newModel =
                    { model
                        | storage = removeVideoFromStorage model.kind model.storage video.id
                    }
            in
            ( newModel, saveVideos (VideoStorage newModel.storage.queue newModel.storage.skipped) )



-- VIEW


queueListItem : String -> Video -> Html Msg
queueListItem _ video =
    ul []
        [ div [ css [ displayFlex ] ]
            [ span [ css [ flexGrow (int 1) ] ] [ text video.title ]
            , button [ css [ flexBasis (pct 20) ], onClick (QueueRemoved video) ] [ text "remove" ]
            ]
        ]


queueList : OrderedDict String Video -> List (Html Msg)
queueList queue =
    OrderedDict.values (OrderedDict.map queueListItem queue)


view : Model -> { title : String, content : Html Msg }
view model =
    { title = ""
    , content =
        ul []
            (queueList
                (case model.kind of
                    Queue ->
                        model.storage.queue

                    Skipped ->
                        model.storage.skipped
                )
            )
    }
